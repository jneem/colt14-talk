<html lang="en">    
<head>
    <meta charset="utf-8">
    <title>Stochastic block model</title>
    <link rel="stylesheet" href="js/reveal.js/css/reveal.min.css">
    <link rel="stylesheet" href="js/reveal.js/css/theme/serif.css" id="theme">    
    <!--Add support for earlier versions of Internet Explorer -->
    <!--[if lt IE 9]>
    <script src="lib/js/html5shiv.js"></script>
    <![endif]-->
</head>
 
<body>
    \(
    \def\R{\mathbf R}
    \def\calG{\mathcal G}
    \)
    <!-- Wrap the entire slide show in a div using the "reveal" class. -->
    <div class="reveal">
        <!-- Wrap all slides in a single "slides" class -->
        <div class="slides">
 
            <!-- ALL SLIDES GO HERE -->
            <!-- Each section element contains an individual slide -->
            <section>
                <h2>Optimal reconstruction in stochastic block models</h1>
                <p>Elchanan Mossel, <em>Joe Neeman</em>, and Allan Sly</p>
            </section>
 
            <section>
                <h3>\(\calG(n, p, q)\)</h3>
                <ol>
                    <li class="fragment"
                        showAction="makeRed('#block-svg [id$=red] ellipse'); makeBlue('#block-svg [id$=blue] ellipse');"
                        hideAction="makeGrey('#block-svg ellipse')">
                        Color the nodes randomly.
                    </li>
                    <li class="fragment"
                        showAction="makeRed('#block-svg [id$=red] path'); makeBlue('#block-svg [id$=blue] path');"
                        hideAction="$('#block-svg path').hide()">
                        If two nodes have the same color, add an edge with probability \(p\).
                    </li>
                    <li class="fragment"
                        showAction="setColor('#block-svg [id$=cross] path', 'purple');"
                        hideAction="$('#block-svg [id$=cross] path').hide()">
                        If two nodes have different colors, add an edge with probability \(q\).
                    </li>
                    <div class="fragment"
                        showAction="makeGrey('#block-svg ellipse, #block-svg path')"
                        hideAction="makeRed('#block-svg [id$=red] path');
                                    makeBlue('#block-svg [id$=blue] path');
                                    makeRed('#block-svg [id$=red] ellipse');
                                    makeBlue('#block-svg [id$=blue] ellipse');
                                    setColor('#block-svg [id$=cross] path', 'purple');">
                    </div>
                <div class="stretch">
                    <svg id="block-svg" width="80%" height="80%"></svg>
                </div>
            </section>
 
            <section id="exact">
                <div>First introduced by Holland, Laskey, Leinhardt in 1983.
                Motivation: discover communities in large networks.</div>

                <blockquote class="fragment" style="font-style:normal">
                    <span style="font-weight:bold">Theorem</span>
                    (Boppana, Dyer/Frieze, Snijders/Nowicki, Condon/Karp, McSherry, Bickel/Chen, &hellip;):
                    <p style="margin-top:1em">
                    There are efficient algorithms for <em>exactly</em> recovering the true colors,
                    provided that \(|p-q|\) is large enough as \(n \to \infty\).
                    </p>
                </blockquote>
            </section>

            <section>
                <div>From now on:</div>
                <h2 class="fragment">\(\calG\left(n, \frac an, \frac bn\right)\)</h2>
                <ul>
                    <li class="fragment">sparsest non-trivial case</li>
                    <li class="fragment"><em>exact</em> recovery is impossible</li>
                </ul>

                <div class="fragment">
                    So, how accurate can we get?
                </div>
            </section>

            <section id="theorem">
                <div>
                    <p>Previously,</p>
                    <blockquote style="font-style:normal">
                        <span style="font-weight:bold;">Theorem</span>
                        (Mossel, N., Sly; Massoulié):<br />
                        Asymptotic accuracy
                        <span style="font-weight:bold;color:red;">
                        better than 50%</span>
                        iff
                        $$
                        \frac{|a-b|}{\sqrt{a+b}} \gt \sqrt{2}.
                        $$
                    </blockquote>
                    <div>(Conjectured by Decelle, Krzakala, Moore, Zdeborova)</div>
                </div>

                <blockquote class="fragment" style="font-style:normal;margin-top:2em;">
                    <span style="font-weight:bold">Main Theorem</span>:
                    We give an algorithm that achieves
                    asymptotically
                    <span style="font-weight:bold;color:red;"> optimal</span>
                    accuracy as \(n \to \infty\), provided that 
                    $$\frac{|a-b|}{\sqrt{a+b}} \gt C.$$
                </blockquote>

            </section>

            <section>
                <h3>Outline</h3>
                <ul>
                    <li class="fragment">Clustering in a block model is harder than the <em>tree reconstruction problem</em>.</li>
                    <li class="fragment">Clustering in a block model is easier than the <em>robust tree reconstruction problem</em>.</li>
                    <li class="fragment">The <em>robust tree reconstruction</em> is just as hard as the <em>tree reconstruction problem</em>.</li>
                </ul>
            </section>

            <section id="tree-reconstruction">
                <h3>Tree reconstruction</h3>
                <ol>
                    <li>Start with a randomly colored node.</li>
                    <li class="fragment"
                        showAction="$('#branching [id*=level0]').show();"
                        hideAction="$('#branching [id*=level0]').hide();">
                        Generate \(\text{Poisson}(a/2)\) children with the same color and
                        \(\text{Poisson}(b/2)\) children with the other color.
                    </li>
                    <li class="fragment"
                        showAction="$('#branching [id*=level1]').show();"
                        hideAction="$('#branching [id*=level1]').hide();">
                        Repeat.
                    </li>
                </ol>
                <div class="stretch" id="branching">
                </div>
                <div class="fragment"
                    showAction="$('#branching [id*=level2]').show();"
                    hideAction="$('#branching [id*=level2]').hide();">
                </div>
                <div class="fragment"
                    showAction="$('#branching [id*=level3]').show();"
                    hideAction="$('#branching [id*=level3]').hide();">
                </div>

                <div class="fragment"
                    showAction="makeGrey('#branching [id*=level0] ellipse');
                                makeGrey('#branching [id*=level1] ellipse');
                                makeGrey('#branching [id*=level2] ellipse');
                                setColor('#branching #rootred ellipse', 'purple')"
                    hideAction="makeRed('#branching [id$=red] ellipse'); makeBlue('#branching [id$=blue] ellipse')">
                    Tree reconstruction problem: given the leaf colors, guess the color of the root.
                </div>
                <div class="fragment"
                    showAction="noisyColors('#branching [id*=level3] ellipse')"
                    hideAction="makeRed('#branching [id*=level3][id$=red] ellipse');
                                makeBlue('#branching [id*=level3][id$=blue] ellipse')">
                    Robust tree reconstruction problem: given <em>noisy observations of</em>
                    the leaf colors, guess the color of the root.
                </div>
                <div class="fragment">For both problems, optimal solution is given by <em>belief propagation</em>.
            </section>

            <section id="non-reconstruction">
                <h3>Graph clustering is harder than tree reconstruction</h3>
                <div id="tree-svg" class="stretch"></div>
                <div class="fragment"
                    showAction="makeGrey('#tree-svg ellipse');
                                makePurple('#tree-svg #bluenode0 ellipse, #tree-svg #bluetarget6 ellipse');"
                    hideAction="colorById('#tree-svg g', 'ellipse');">
                </div>
                <div class="fragment"
                    showAction="makeBlue('#tree-svg #bluetarget6 ellipse');"
                    hideAction="makePurple('#tree-svg #bluetarget6 ellipse');">
                </div>
                <div class="fragment"
                    showAction="colorById('#tree-svg [id$=4]', 'ellipse');"
                    hideAction="makeGrey('#tree-svg [id$=4] ellipse');">
                </div>
                <div class="fragment"
                    showAction="makeGrey('#tree-svg #bluetarget6 ellipse');"
                    hideAction="makeBlue('#tree-svg #bluetarget6 ellipse');">
                </div>

                <div class="fragment">
                    This is (approximately) the tree reconstruction problem!
                </div>
            </section>

            <section id="reconstruction">
                <h3>Graph clustering is easier than robust tree reconstruction</h3>
                <div class="stretch" id="tree-svg2"></div>
                <ol>
                    <li class="fragment"
                        showAction="$('#tree-svg2 [id$=3]').find('ellipse, path').hide();
                                    $('#tree-svg2 [id$=2]').find('ellipse, path').hide();
                                    $('#tree-svg2 [id$=1]').find('ellipse, path').hide();
                                    $('#tree-svg2 [id$=0]').find('ellipse, path').hide();"
                        hideAction="$('#tree-svg2').find('ellipse, path').show();">
                        throw away a neighborhood
                    </li>
                    <li class="fragment"
                        showAction="noisyColors('#tree-svg2 [id$=4] ellipse');
                                    noisyColors('#tree-svg2 [id$=5] ellipse');
                                    noisyColors('#tree-svg2 [id$=6] ellipse');
                                    noisyColors('#tree-svg2 [id$=7] ellipse');
                                    noisyColors('#tree-svg2 [id$=8] ellipse');"
                        hideAction="makeGrey('#tree-svg2 [id$=4] ellipse');
                                    makeGrey('#tree-svg2 [id$=5] ellipse');
                                    makeGrey('#tree-svg2 [id$=6] ellipse');
                                    makeGrey('#tree-svg2 [id$=7] ellipse');
                                    makeGrey('#tree-svg2 [id$=8] ellipse');">
                        run a sub-optimal algorithm on the rest
                    </li>
                    <li class="fragment"
                        showAction="$('#tree-svg2').find('ellipse, path').show();"
                        hideAction="$('#tree-svg2 [id$=3]').find('ellipse, path').hide();
                                    $('#tree-svg2 [id$=2]').find('ellipse, path').hide();
                                    $('#tree-svg2 [id$=1]').find('ellipse, path').hide();
                                    $('#tree-svg2 [id$=0]').find('ellipse, path').hide();">
                        use <em>robust tree reconstruction</em> to guess the color of the root
                    </li>
                    <li class="fragment"
                        showAction="makeGrey('#tree-svg2 ellipse');
                                    makePurple('#tree-svg2 #bluetarget6 ellipse');"
                        hideAction="noisyColors('#tree-svg2 [id$=4] ellipse');
                                    noisyColors('#tree-svg2 [id$=5] ellipse');
                                    noisyColors('#tree-svg2 [id$=6] ellipse');
                                    noisyColors('#tree-svg2 [id$=7] ellipse');
                                    noisyColors('#tree-svg2 [id$=8] ellipse');
                                    makePurple('#tree-svg2 #bluenode0 ellipse');">
                        repeat for other vertices
                    </li>
                    <div class="fragment" style="margin-top:1em;">
                        Break red/blue symmetry by fixing a node of high degree and always
                        trying to infer its color.
                    </div>
                </ol>
            </section>

            <section id="equivalence">
                <h3>Robust tree reconstruction is exactly as hard as tree reconstruction</h3>

                <blockquote style="font-style:normal;">
                    <span style="font-weight:bold">Theorem</span>:
                    Consider robust tree reconstruction where the observations are flipped
                    with probability \(\delta \lt 1/2\). If \(\frac{|a-b|}{\sqrt{a+b}} \gt C \)
                    then the asymptotic reconstruction accuracy is independent of \(\delta\).
                </blockquote>

                <ul>
                    <li class="fragment">main technical result</li>
                    <li class="fragment">this is the only place where the condition
                        \(\frac{|a-b|}{\sqrt{a+b}} \gt C\) enters</li>
                    <li class="fragment">proof by coupling <em>belief propagation</em> for
                        noisy and non-noisy observations</li>
                <ul>

                <blockquote style="font-style:normal;margin-top:1em" class="fragment">
                    <span style="font-weight:bold">Conjecture</span>:
                    For any \(a, b\), the reconstruction accuracy is independent of \(\delta\).
                </blockquote>
                <blockquote style="font-style:normal;margin-top:1em" class="fragment">
                    <span style="font-weight:bold">Remark</span>:
                    If \(\frac{|a-b|}{\sqrt{a+b}} \le \sqrt{2}\) then
                    the accuracy is independent of \(\delta\).
                </blockquote>
            </section>

            <section>
                <h3>Summary</h3>
                <ul>
                    <li class="fragment">optimal algorithm for learning block models with two balanced colors</li>
                    <li class="fragment">based on local applications of belief propagation</li>
                    <li class="fragment">main technical part: irrelevance of noise for tree reconstruction</li>
                </ul>

                <h3 style="margin-top:1em" class="fragment">Open problems</h3>
                <div class="fragment">Unbalanced colors? More than two colors?</div>
                <ul>
                    <li class="fragment">the previous conjecture is false in general</li>
                    <li class="fragment"><span style="font-weight:bold">Conjecture</span>: our
                    algorithm is optimal among <em>computationally efficient</em> algorithms.
                </ul>

                <h3 class="fragment" style="margin-top:1em">Thank you!</h3>
            </section>

    </div>
    <script src="js/reveal.js/lib/js/head.min.js"></script>
    <script src="js/reveal.js/js/reveal.min.js"></script>
    <script src="js/jquery-2.1.0.min.js"></script>
    <script src="js/reveal-control/reveal-control.js"></script>
 
    <script>
        $(document).ready(function()
        {
            $('#block-svg').load('block-model.svg', function() {
                $('#block-svg path').hide();
                makeGrey('#block-svg ellipse');
            });

            $('#branching').load('branching.svg', function() {
                $('#branching [id*=level0]').hide();
                $('#branching [id*=level1]').hide();
                $('#branching [id*=level2]').hide();
                $('#branching [id*=level3]').hide();
            });

            $('#tree-svg').load('block-model-tree.svg');
            $('#tree-svg2').load('block-model-tree.svg', function() {
                makeGrey('#tree-svg2 ellipse');
                makePurple('#tree-svg2 #bluenode0 ellipse');
            });
        });

        function setEltColor(elt, color) {
            elt.attr({
                fill: color,
                stroke: color
            });
            elt.show();
        }

        function setColor(selector, color) {
            setEltColor($(selector), color);
        }

        function makeRed(selector) { setColor(selector, 'red'); }
        function makeBlue(selector) { setColor(selector, 'blue'); }
        function makePurple(selector) { setColor(selector, 'purple'); }
        function makeGrey(selector) { setColor(selector, 'grey'); }

        function colorById(selector, subselector) {
            setEltColor($(selector).filter('[id*=red]').find(subselector), 'red');
            setEltColor($(selector).filter('[id*=blue]').find(subselector), 'blue');
        }

        function noisyColors(selector) {
            var red = 'rgba(255, 0, 0, 0.3)';
            var blue = 'rgba(0, 0, 255, 0.3)';
            $(selector).each(function(i, elt) {
                setColor(elt, (Math.random() < 0.5) ? red : blue);
            });
        }

        // Required, even if empty.
        Reveal.initialize({
            transition: 'fade',
            transitionSpeed: 'fast',
            controls: false,
            progress: false,
            overview: false,

            math: {
                mathjax: 'js/MathJax/MathJax.js',
                config: 'TeX-AMS_HTML-full'  // See http://docs.mathjax.org/en/latest/config-files.html
            },

            dependencies: [
                { src: 'js/reveal.js/plugin/math/math.js', async: true }
            ]
        });
    </script>
</body>
</html>

