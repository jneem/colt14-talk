import pygraphviz as pgv
from numpy.random import poisson
import random

def DrawTree(a, b, r=5):
    g = pgv.AGraph()
    g.graph_attr['root'] = 'root'
    g.graph_attr['rankdir'] = 'BT'
    g.node_attr['shape'] = 'circle'
    g.node_attr['width'] = 0.2
    g.node_attr['style'] = 'filled'
    g.node_attr['label'] = ' '
    g.edge_attr['penwidth'] = 0.8
    #ag.graph_attr['splines'] = False

    g.add_node('root', color = 'red', label='', id='rootred')
    last_level = ['root']
    g.add_subgraph(['root'], rank='same')

    def flip(color):
        return 'red' if color == 'blue' else 'blue'

    for level in range(r):
        next_level = []
        for parent in last_level:
            parent_color = g.get_node(parent).attr['color']
            other_color = flip(parent_color)

            equal = [parent + '/' + str(i) for i in range(poisson(a/2) + 1)]
            other = [parent + '/' + str(len(equal) + i) for i in range(poisson(b/2))]
            
            for u in equal:
                g.add_node(u, color = parent_color, label='', id=(u + 'level' + str(level) + parent_color))
                g.add_edge(u, parent, id=(u + 'level' + str(level)))

            for u in other:
                g.add_node(u, color = other_color, label='', id=(u + 'level' + str(level) + other_color))
                g.add_edge(u, parent, id=(u + 'level' + str(level)))

            next_level += equal + other
        g.add_subgraph(next_level, rank='same')
        last_level = next_level

    g.write('branching.dot')
    g.draw('branching.svg', prog='dot')

if __name__ == '__main__':
    DrawTree(1.2, 1.0, 4)

