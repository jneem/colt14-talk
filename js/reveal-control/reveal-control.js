'use strict';

// TODO: package this up in a singleton object.

var activeController = null;

function runAction(action)
{
    if (activeController != null) {
        eval('controller.' + action);
    } else {
        console.log(action);
        eval(action);
    }
}

function manageSlide(event) {
    // Finish off the previous slide.
    var elt = $(event.previousSlide);
    var action = elt.attr('hideAction');
    if (action != null) {
        runAction(action);
    }
    if (activeController != null && 'stop' in activeController) {
        activeController.stop();
    }


    activeController = null;

    var elt = $(event.currentSlide);
    var controllerName = elt.attr('controller');
    var controller = null;

    if (controllerName != null) {
        controller = Object.create(eval(controllerName + '.prototype'));

        controller.init(elt);
        activeController = controller;
    }

    var action = elt.attr('showAction');
    if (action != null) {
        runAction(action);
    }
}

function manageFragment(event, attribute) {
    var frag = $(event.fragment);
    var action = frag.attr(attribute);

    if (action != null) {
        runAction(action);
    }
}

function showFragment(event) {
    manageFragment(event, 'showAction');
}

function hideFragment(event) {
    manageFragment(event, 'hideAction');
}

$(document).ready(function() {
    Reveal.addEventListener('slidechanged', manageSlide);
    Reveal.addEventListener('ready', manageSlide);
    Reveal.addEventListener('fragmentshown', showFragment);
    Reveal.addEventListener('fragmenthidden', hideFragment);
});

